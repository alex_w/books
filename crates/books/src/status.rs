use analyzer::Status;
use parser::{Amount, Currency};
use std::{
    error::Error,
    io::{self, Write},
};

pub fn status(writer: &mut impl Write, source: &str) -> Result<(), Box<dyn Error>> {
    let parser = parser::Parser::new(source);
    let mut status = Status::default();
    for transaction in parser {
        status.add_transaction(&transaction?);
    }

    write_status(writer, &status)?;

    Ok(())
}

struct DisplayAmount {
    value: String,
    currency: Currency,
    negative: bool,
}

impl From<&Amount> for DisplayAmount {
    fn from(amount: &Amount) -> Self {
        Self {
            value: format!("{:.2}", amount.value.abs()),
            currency: amount.currency.clone(),
            negative: amount.value.is_sign_negative(),
        }
    }
}

fn write_status(writer: &mut impl Write, status: &Status) -> io::Result<()> {
    let total_assets = status.total_assets();
    let total_liabilities = status.total_liabilities();
    let net_worth = status.net_worth();

    let total_assets: Vec<_> = total_assets
        .amounts
        .iter()
        .map(DisplayAmount::from)
        .collect();
    let total_liabilities: Vec<_> = total_liabilities
        .amounts
        .iter()
        .map(DisplayAmount::from)
        .collect();
    let net_worth: Vec<_> = net_worth.amounts.iter().map(DisplayAmount::from).collect();
    let max_value_length = total_assets
        .iter()
        .chain(total_liabilities.iter())
        .chain(net_worth.iter())
        .map(|amount| amount.value.len())
        .max()
        .unwrap_or(0);

    write!(writer, "Total assets:      ")?;
    write_amounts(writer, total_assets, max_value_length)?;

    write!(writer, "Total liabilities: ")?;
    write_amounts(writer, total_liabilities, max_value_length)?;

    write!(writer, "Net Worth:         ")?;
    write_amounts(writer, net_worth, max_value_length)?;

    Ok(())
}

fn write_amounts(
    writer: &mut impl Write,
    amounts: Vec<DisplayAmount>,
    max_value_length: usize,
) -> io::Result<()> {
    let length = amounts.len();
    for (i, amount) in amounts.iter().enumerate() {
        let maybe_minus_sign = if amount.negative { "-" } else { " " };
        if i + 1 == length {
            write!(
                writer,
                "{}{maybe_minus_sign}{:>max_value_length$}",
                amount.currency, amount.value
            )?;
        } else {
            write!(
                writer,
                "{}{maybe_minus_sign}{:>max_value_length$}, ",
                amount.currency, amount.value
            )?;
        }
    }
    writeln!(writer)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty_source_empty_status() -> Result<(), Box<dyn Error>> {
        let source = "";
        let mut output = Vec::new();

        status(&mut output, source)?;

        let expected = "Total assets:      \nTotal liabilities: \nNet Worth:         \n";
        assert_eq!(expected.as_bytes(), output);

        Ok(())
    }

    #[test]
    fn valid_source_correct_status() -> Result<(), Box<dyn Error>> {
        let source = "
            2022-03-30 opening balances
                assets:piggy bank:account £ 500
                equity:opening balances   £-500
            
            2022-03-30 shopping
                expenses:food:groceries   £ 44.44
                assets:piggy bank:account £-44.44
            
            2022-03-30 salary
                assets:piggy bank:account £ 500
                income:salary             £-500
            
            2022-03-30 loan
                assets:piggy bank:account   £ 250.50
                liabilities:piggy bank:loan £-250.50
        ";
        let mut output = Vec::new();

        status(&mut output, source)?;

        let expected = "Total assets:      £ 1206.06\nTotal liabilities: £- 250.50\nNet Worth:         £  955.56\n";
        assert_eq!(expected.as_bytes(), output);

        Ok(())
    }
}
