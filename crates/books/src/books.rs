use std::path::PathBuf;

/// Accounting tool for working with plain text ledger files.
#[derive(clap::Parser)]
#[clap(name = "books", version)]
pub struct Books {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(clap::Subcommand)]
pub enum Commands {
    /// Status of the ledger.
    Status {
        /// Path for ledger file.
        #[clap(required = true)]
        file: PathBuf,
    },
}
