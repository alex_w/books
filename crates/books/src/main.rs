mod books;
mod status;

use crate::books::{Books, Commands};
use status::status;
use std::{error::Error, fs, io};

fn main() -> Result<(), Box<dyn Error>> {
    use clap::Parser;

    let args = Books::parse();

    match args.command {
        Commands::Status { file } => {
            let file = fs::read_to_string(file)?;
            let mut stdout = io::stdout();

            status(&mut stdout, &file)?;
        }
    }

    Ok(())
}
