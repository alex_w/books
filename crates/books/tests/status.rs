use assert_cmd::Command;
use assert_fs::fixture::FileWriteStr;
use predicates::prelude::predicate;
use std::error::Error;

#[test]
fn file_doesnt_exist() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin("books")?;

    cmd.arg("status")
        .arg("file/does/not/exist")
        .assert()
        .failure()
        .stderr(predicate::str::contains("No such file or directory"));

    Ok(())
}

#[test]
fn status() -> Result<(), Box<dyn Error>> {
    let file = assert_fs::NamedTempFile::new("status.ledger")?;
    file.write_str(
        "2022-03-28 opening balances
            assets:piggy bank:account £ 500
            equity:opening balances   £-500
        
        2022-03-28 shopping
            expenses:food:groceries   £ 44.44
            assets:piggy bank:account £-44.44
        
        2022-03-28 salary
            assets:piggy bank:account £ 500
            income:salary             £-500
        
        2022-03-28 loan
            assets:piggy bank:account   £ 250.50
            liabilities:piggy bank:loan £-250.50",
    )?;
    let mut cmd = Command::cargo_bin("books")?;

    cmd.arg("status")
        .arg(file.path())
        .assert()
        .success()
        .stdout(predicate::str::contains(
        "Total assets:      £ 1206.06\nTotal liabilities: £- 250.50\nNet Worth:         £  955.56",
    ));

    Ok(())
}

#[test]
fn invalid_date() -> Result<(), Box<dyn Error>> {
    let file = assert_fs::NamedTempFile::new("status.ledger")?;
    file.write_str("2022-03")?;
    let mut cmd = Command::cargo_bin("books")?;

    cmd.arg("status")
        .arg(file.path())
        .assert()
        .failure()
        .stderr(predicate::str::contains("UnexpectedToken"));

    Ok(())
}
