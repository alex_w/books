use clap::IntoApp;
use clap_complete::{generate_to, shells};
use std::{env, ffi::OsString, io};

fn main() -> io::Result<()> {
    let out_dir = env::var_os("OUT_DIR").ok_or(io::ErrorKind::NotFound)?;

    generate_completions(&out_dir)?;

    Ok(())
}

fn generate_completions(out_dir: &OsString) -> io::Result<()> {
    #[path = "src/books.rs"]
    mod books;

    let mut books = books::Books::command();

    generate_to(shells::Bash, &mut books, "books", out_dir)?;
    generate_to(shells::Elvish, &mut books, "books", out_dir)?;
    generate_to(shells::Fish, &mut books, "books", out_dir)?;
    generate_to(shells::PowerShell, &mut books, "books", out_dir)?;
    generate_to(shells::Zsh, &mut books, "books", out_dir)?;

    Ok(())
}
