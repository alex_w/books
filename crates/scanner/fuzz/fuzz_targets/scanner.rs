#![no_main]
use libfuzzer_sys::fuzz_target;
use scanner::Scanner;

fuzz_target!(|source: &str| {
    let scanner = Scanner::new(source);

    let source_len = source.len();
    let mut previous_token_end = 0;
    let mut total_token_len = 0;
    for token in scanner {
        assert!(token.start >= previous_token_end);
        assert!(token.len > 0);

        total_token_len += token.len;
        assert!(source_len >= total_token_len);

        previous_token_end = token.start + token.len;
        assert!(source_len >= previous_token_end);
    }
});
