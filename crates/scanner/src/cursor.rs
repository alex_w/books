use std::str::Chars;

pub struct Cursor<'a> {
    source: Chars<'a>,
    initial_len: usize,
}

impl<'a> Cursor<'a> {
    pub fn new(source: &'a str) -> Self {
        Self {
            source: source.chars(),
            initial_len: source.len(),
        }
    }

    pub fn bump(&mut self) -> Option<char> {
        self.source.next()
    }

    pub fn first(&self) -> Option<char> {
        self.source.clone().next()
    }

    pub fn eat_while(&mut self, predicate: fn(char) -> bool) {
        while let Some(c) = self.first() {
            if predicate(c) {
                self.bump();
            } else {
                return;
            }
        }
    }

    pub fn len_consumed(&self) -> usize {
        self.initial_len - self.source.as_str().len()
    }

    pub fn bump_if(&mut self, predicate: impl Fn(char) -> bool) -> bool {
        if self.first().map(predicate).unwrap_or(false) {
            self.bump();

            true
        } else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_returns_cursor() {
        let source = "source";

        Cursor::new(source);
    }

    #[test]
    fn bump_when_empty_returns_none() {
        let source = "";
        let mut cursor = Cursor::new(source);

        let token = cursor.bump();

        assert_eq!(None, token);
    }

    #[test]
    fn bump_returns_next_char() {
        let source = "abc";
        let mut cursor = Cursor::new(source);

        let token = cursor.bump();

        assert_eq!(Some('a'), token);
    }

    #[test]
    fn bump_consumes_char() {
        let source = "abc";
        let mut cursor = Cursor::new(source);
        cursor.bump();

        let token = cursor.bump();

        assert_eq!(Some('b'), token);
    }

    #[test]
    fn first_returns_first_char() {
        let source = "abc";
        let cursor = Cursor::new(source);

        let token = cursor.first();

        assert_eq!(Some('a'), token);
    }

    #[test]
    fn first_does_not_consume_char() {
        let source = "abc";
        let mut cursor = Cursor::new(source);

        cursor.first();
        let token = cursor.bump();

        assert_eq!(Some('a'), token);
    }

    #[test]
    fn eat_while_false_does_not_conusme_char() {
        let source = "abc";
        let mut cursor = Cursor::new(source);
        let predicate = |_| false;

        cursor.eat_while(predicate);
        let token = cursor.bump();

        assert_eq!(Some('a'), token);
    }

    #[test]
    fn eat_while_true_consumes_all_chars() {
        let source = "abc";
        let mut cursor = Cursor::new(source);
        let predicate = |_| true;

        cursor.eat_while(predicate);
        let token = cursor.bump();

        assert_eq!(None, token);
    }

    #[test]
    fn eat_while_a_consumes_a_char() {
        let source = "abc";
        let mut cursor = Cursor::new(source);
        let predicate = |c| c == 'a';

        cursor.eat_while(predicate);
        let token = cursor.bump();

        assert_eq!(Some('b'), token);
    }

    #[test]
    fn len_consumed_at_start_returns_zero() {
        let source = "abc";
        let cursor = Cursor::new(source);

        let len_consumed = cursor.len_consumed();

        assert_eq!(0, len_consumed);
    }

    #[test]
    fn len_consumed_after_consume_a_char_returns_one() {
        let source = "abc";
        let mut cursor = Cursor::new(source);
        cursor.bump();

        let len_consumed = cursor.len_consumed();

        assert_eq!(1, len_consumed);
    }

    #[test]
    fn bump_if_does_not_bump_for_false() {
        let source = "abc";
        let mut cursor = Cursor::new(source);
        let predicate = |c| matches!(c, 'b');

        let bumped = cursor.bump_if(predicate);

        let token = cursor.bump();
        assert!(!bumped);
        assert_eq!(Some('a'), token);
    }

    #[test]
    fn bump_if_does_bump_for_true() {
        let source = "abc";
        let mut cursor = Cursor::new(source);
        let predicate = |c| matches!(c, 'a');

        let bumped = cursor.bump_if(predicate);

        let token = cursor.bump();
        assert!(bumped);
        assert_eq!(Some('b'), token);
    }
}
