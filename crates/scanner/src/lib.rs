mod cursor;
mod token;

pub use token::{Token, TokenKind};

use cursor::Cursor;

/// Scanner that iterates over tokens in source.
pub struct Scanner<'a> {
    source: Cursor<'a>,
}

impl<'a> Scanner<'a> {
    /// Create a new scanner producing tokens from provided source.
    pub fn new(source: &'a str) -> Self {
        let source = Cursor::new(source);

        Self { source }
    }

    fn next_token(&mut self) -> Option<Token> {
        self.source.eat_while(is_whitespace);

        let start = self.source.len_consumed();
        let next = self.source.bump()?;
        let kind = match next {
            '\n' => TokenKind::EOL,
            '-' => TokenKind::Dash,
            ':' => TokenKind::Colon,
            '£' => TokenKind::GreatBritishPound,
            c if is_digit(c) => self.number(),
            c if is_alpha(c) => self.word(c),
            _ => TokenKind::Unknown,
        };
        let len = self.source.len_consumed() - start;

        Some(Token { kind, start, len })
    }

    fn number(&mut self) -> TokenKind {
        self.source.eat_while(is_digit);

        if self.source.bump_if(|c| matches!(c, '.')) {
            self.source.eat_while(is_digit);

            TokenKind::Float
        } else {
            TokenKind::Integer
        }
    }

    fn word(&mut self, c: char) -> TokenKind {
        match c {
            'a' => match self.source.first() {
                Some('s') => {
                    self.source.bump();

                    self.check_keyword(['s', 'e', 't', 's'], TokenKind::Assets)
                }
                c if is_end_of_word(c) => TokenKind::Assets,
                _ => self.finish_word(),
            },
            'e' => match self.source.first() {
                Some('q') => {
                    self.source.bump();

                    self.check_keyword(['u', 'i', 't', 'y'], TokenKind::Equity)
                }
                Some('x') => {
                    self.source.bump();

                    self.check_keyword(['p', 'e', 'n', 's', 'e', 's'], TokenKind::Expenses)
                }
                c if is_end_of_word(c) => TokenKind::Equity,
                _ => self.finish_word(),
            },
            'i' => match self.source.first() {
                Some('n') => {
                    self.source.bump();

                    self.check_keyword(['c', 'o', 'm', 'e'], TokenKind::Income)
                }
                c if is_end_of_word(c) => TokenKind::Income,
                _ => self.finish_word(),
            },
            'l' => match self.source.first() {
                Some('i') => {
                    self.source.bump();

                    self.check_keyword(
                        ['a', 'b', 'i', 'l', 'i', 't', 'i', 'e', 's'],
                        TokenKind::Liabilities,
                    )
                }
                c if is_end_of_word(c) => TokenKind::Liabilities,
                _ => self.finish_word(),
            },
            'x' => match self.source.first() {
                c if is_end_of_word(c) => TokenKind::Expenses,
                _ => self.finish_word(),
            },
            _ => self.finish_word(),
        }
    }

    fn check_keyword(
        &mut self,
        expected: impl IntoIterator<Item = char>,
        kind: TokenKind,
    ) -> TokenKind {
        for expected in expected {
            if !self.source.bump_if(|next| next == expected) {
                return self.finish_word();
            }
        }

        if self.source.bump_if(is_alpha) {
            self.finish_word()
        } else {
            kind
        }
    }

    fn finish_word(&mut self) -> TokenKind {
        self.source.eat_while(is_alpha);

        TokenKind::Word
    }
}

impl Iterator for Scanner<'_> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        self.next_token()
    }
}

fn is_whitespace(c: char) -> bool {
    matches!(c, ' ' | '\t' | '\r')
}

fn is_digit(c: char) -> bool {
    matches!(c, '0'..='9')
}

fn is_alpha(c: char) -> bool {
    matches!(c, 'a'..='z' | 'A'..='Z' | '_')
}

fn is_end_of_word(c: Option<char>) -> bool {
    c.map(|c| !is_alpha(c)).unwrap_or(true)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_returns_scanner() {
        let source = "source";

        Scanner::new(source);
    }

    #[test]
    fn next_empty_source_returns_none() {
        let source = "";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        assert_eq!(None, token);
    }

    #[test]
    fn next_ignores_leading_whitespace() {
        let source = " \t \r";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        assert_eq!(None, token);
    }

    #[test]
    fn next_on_newline_returns_eol() {
        let source = "\n";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::EOL,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_on_carriage_return_newline_returns_eol() {
        let source = "\r\n";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::EOL,
            start: 1,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_on_integer_returns_integer() {
        let source = "42";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Integer,
            start: 0,
            len: 2,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_on_decimal_returns_float() {
        let source = "4.2";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Float,
            start: 0,
            len: 3,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_on_dash_returns_dash() {
        let source = "-";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Dash,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_colon_returns_colon() {
        let source = ":";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Colon,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_pound_sign_returns_great_british_pound() {
        let source = "£";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::GreatBritishPound,
            start: 0,
            len: 2,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_word_returns_word() {
        let source = "word";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 4,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_upper_case_word_returns_word() {
        let source = "WORD";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 4,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_word_with_underscore_returns_word() {
        let source = "_w_o_r_d_";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 9,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_two_words_returns_two_words() {
        let source = "two words";
        let scanner = Scanner::new(source);

        let tokens: Vec<Token> = scanner.collect();

        let expected = vec![
            Token {
                kind: TokenKind::Word,
                start: 0,
                len: 3,
            },
            Token {
                kind: TokenKind::Word,
                start: 4,
                len: 5,
            },
        ];
        assert_eq!(expected, tokens);
    }

    #[test]
    fn next_assets_returns_assets() {
        let source = "assets";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Assets,
            start: 0,
            len: 6,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_a_returns_asset() {
        let source = "a";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Assets,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_a_followed_by_word_returns_asset() {
        let source = "a word";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Assets,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_assetsword_returns_word() {
        let source = "assetsword";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 10,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_at_returns_word() {
        let source = "at";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 2,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_equity_returns_equity() {
        let source = "equity";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Equity,
            start: 0,
            len: 6,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_e_returns_equity() {
        let source = "e";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Equity,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_e_followed_by_word_returns_equity() {
        let source = "e word";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Equity,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_equityword_returns_word() {
        let source = "equityword";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 10,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_et_returns_word() {
        let source = "et";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 2,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_expenses_returns_expenses() {
        let source = "expenses";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Expenses,
            start: 0,
            len: 8,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_x_returns_expenses() {
        let source = "x";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Expenses,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_x_followed_by_word_returns_expenses() {
        let source = "x word";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Expenses,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_expensesword_returns_word() {
        let source = "expensesword";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 12,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_xt_returns_word() {
        let source = "xt";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 2,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_income_returns_income() {
        let source = "income";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Income,
            start: 0,
            len: 6,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_i_returns_income() {
        let source = "i";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Income,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_i_followed_by_word_returns_income() {
        let source = "i word";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Income,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_incomeword_returns_word() {
        let source = "incomeword";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 10,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_it_returns_word() {
        let source = "it";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 2,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_liabilities_returns_liabilities() {
        let source = "liabilities";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Liabilities,
            start: 0,
            len: 11,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_l_returns_liabilities() {
        let source = "l";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Liabilities,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_l_followed_by_word_returns_liabilities() {
        let source = "l word";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Liabilities,
            start: 0,
            len: 1,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_liabilitiesword_returns_word() {
        let source = "liabilitiesword";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 15,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_lt_returns_word() {
        let source = "lt";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Word,
            start: 0,
            len: 2,
        });
        assert_eq!(expected, token);
    }

    #[test]
    fn next_emoji_returns_unknown() {
        let source = "😀";
        let mut scanner = Scanner::new(source);

        let token = scanner.next();

        let expected = Some(Token {
            kind: TokenKind::Unknown,
            start: 0,
            len: 4,
        });
        assert_eq!(expected, token);
    }
}
