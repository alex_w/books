use std::fmt::{self, Display};

/// Token that describes the type of a section of source.
#[derive(Debug, PartialEq)]
pub struct Token {
    /// Type of the token.
    pub kind: TokenKind,
    /// Start of the token in the source.
    pub start: usize,
    /// Length of the token in the source.
    pub len: usize,
}

/// Type of the lexeme.
#[derive(Clone, Debug, PartialEq)]
pub enum TokenKind {
    /// End of line `\n`.
    EOL,
    /// Integer number for example `42`.
    Integer,
    /// Float number for example `4.2`.
    Float,
    /// Dash `-`.
    Dash,
    /// Colon `:`.
    Colon,
    /// Great British pound `£`.
    GreatBritishPound,
    /// Word for example `word`, `WORD` or `_word`.
    Word,
    /// An account for assets.
    Assets,
    /// An account for equity.
    Equity,
    /// An account for expenses.
    Expenses,
    /// An account for income.
    Income,
    /// An account for liabilities.
    Liabilities,
    /// An unknown token.
    Unknown,
}

impl Display for TokenKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::EOL => write!(f, "end of line"),
            Self::Integer => write!(f, "integer"),
            Self::Float => write!(f, "float"),
            Self::Dash => write!(f, "-"),
            Self::Colon => write!(f, ":"),
            Self::GreatBritishPound => write!(f, "£"),
            Self::Word => write!(f, "word"),
            Self::Assets => write!(f, "assets"),
            Self::Equity => write!(f, "equity"),
            Self::Expenses => write!(f, "expenses"),
            Self::Income => write!(f, "income"),
            Self::Liabilities => write!(f, "liabilities"),
            Self::Unknown => write!(f, "unknown"),
        }
    }
}
