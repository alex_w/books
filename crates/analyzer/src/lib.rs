use parser::{AccountKind, Amount, Transaction};

#[derive(Clone, Default, Debug, PartialEq)]
pub struct Amounts {
    pub amounts: Vec<Amount>,
}

impl Amounts {
    fn add_amount(&mut self, amount: &Amount) {
        let total = self
            .amounts
            .iter_mut()
            .find(|total| total.currency == amount.currency);

        if let Some(total) = total {
            total.value += amount.value;
        } else {
            self.amounts.push(amount.clone());
        }
    }

    fn add(&mut self, amounts: &Amounts) {
        for amount in &amounts.amounts {
            self.add_amount(amount);
        }
    }
}

#[derive(Default, Debug, PartialEq)]
pub struct Status {
    assets: Amounts,
    liabilities: Amounts,
}

impl Status {
    pub fn add_transaction(&mut self, transaction: &Transaction) {
        for posting in &transaction.postings {
            match posting.account.kind {
                AccountKind::Assets => self.assets.add_amount(&posting.amount),
                AccountKind::Liabilities => self.liabilities.add_amount(&posting.amount),
                _ => {}
            }
        }
    }

    pub fn net_worth(&self) -> Amounts {
        let mut result = self.assets.clone();
        result.add(&self.liabilities);

        result
    }

    pub fn total_assets(&self) -> Amounts {
        self.assets.clone()
    }

    pub fn total_liabilities(&self) -> Amounts {
        self.liabilities.clone()
    }
}

#[cfg(test)]
mod tests {
    use parser::Parser;

    use super::*;

    #[test]
    fn net_worth_returns_correct_value() {
        let status = status_from_source(
            "2022-03-20 transaction
                assets:piggy bank:account £ 1100
                equity:opening balance    £- 100
                expenses:food:groceries   £  100
                liabilities:loan          £- 100
                income:salary             £-1000",
        );

        let net_worth = status.net_worth();

        let expected = Amounts {
            amounts: vec![Amount {
                currency: parser::Currency::GreatBritishPound,
                value: 1000.,
            }],
        };
        assert_eq!(expected, net_worth);
    }

    #[test]
    fn total_assets_returns_correct_value() {
        let status = status_from_source(
            "2022-03-20 transaction
                assets:piggy bank:account £ 1000
                equity:opening balance    £-1000",
        );

        let total_assets = status.total_assets();

        let expected = Amounts {
            amounts: vec![Amount {
                currency: parser::Currency::GreatBritishPound,
                value: 1000.,
            }],
        };
        assert_eq!(expected, total_assets);
    }

    #[test]
    fn total_liabilities_returns_correct_value() {
        let status = status_from_source(
            "2022-03-20 transaction
                liabilities:loan       £-1000
                equity:opening balance £ 1000",
        );

        let total_liabilities = status.total_liabilities();

        let expected = Amounts {
            amounts: vec![Amount {
                currency: parser::Currency::GreatBritishPound,
                value: -1000.,
            }],
        };
        assert_eq!(expected, total_liabilities);
    }

    fn parse_transaction(source: &str) -> Transaction {
        Parser::new(source)
            .next()
            .expect("No transaction found.")
            .expect("Failed to parse transaction.")
    }

    fn status_from_source(source: &str) -> Status {
        let transaction = parse_transaction(source);
        let mut status = Status::default();
        status.add_transaction(&transaction);

        status
    }
}
