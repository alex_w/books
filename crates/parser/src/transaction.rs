use chrono::NaiveDate;
use std::fmt::{self, Display};

#[derive(Debug, PartialEq)]
pub struct Transaction<'a> {
    pub date: NaiveDate,
    pub description: &'a str,
    pub postings: Vec<Posting<'a>>,
}

#[derive(Debug, PartialEq)]
pub struct Posting<'a> {
    pub account: Account<'a>,
    pub amount: Amount,
}

#[derive(Debug, PartialEq)]
pub struct Account<'a> {
    pub name: &'a str,
    pub kind: AccountKind,
}

#[derive(Debug, PartialEq)]
pub enum AccountKind {
    Assets,
    Equity,
    Expenses,
    Income,
    Liabilities,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Amount {
    pub currency: Currency,
    pub value: f64,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Currency {
    GreatBritishPound,
}

impl Display for Currency {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::GreatBritishPound => write!(f, "£"),
        }
    }
}
