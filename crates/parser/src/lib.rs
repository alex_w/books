pub mod error;

mod transaction;

pub use crate::error::{ParserError, ParserErrorKind, Span};
pub use crate::transaction::{Account, AccountKind, Amount, Currency, Posting, Transaction};

use chrono::NaiveDate;
use error::Result;
use scanner::{Scanner, Token, TokenKind};
use std::iter::Peekable;

pub struct Parser<'a> {
    source: &'a str,
    tokens: Peekable<Scanner<'a>>,
}

impl<'a> Parser<'a> {
    pub fn new(source: &'a str) -> Self {
        Self {
            source,
            tokens: Scanner::new(source).peekable(),
        }
    }

    fn next_transaction(&mut self) -> Option<Result<Transaction<'a>>> {
        if self.tokens.peek().is_none() {
            None
        } else {
            Some(self.transaction())
        }
    }

    fn transaction(&mut self) -> Result<Transaction<'a>> {
        self.eat_while_eol();
        let date = self.date()?;
        let description = self.description();
        let postings = self.postings()?;

        let transaction = Transaction {
            date,
            description,
            postings,
        };

        Ok(transaction)
    }

    fn date(&mut self) -> Result<NaiveDate> {
        let year = self.expect(&[TokenKind::Integer])?;
        self.expect(&[TokenKind::Dash])?;
        let month = self.expect(&[TokenKind::Integer])?;
        self.expect(&[TokenKind::Dash])?;
        let day = self.expect(&[TokenKind::Integer])?;

        let start = year.start;
        let len = year.len + month.len + day.len + 2;
        let date = &self.source[start..start + len];

        NaiveDate::parse_from_str(date, "%Y-%m-%d").map_err(|_| ParserError {
            kind: ParserErrorKind::ParseDateError,
            span: Span { start, len },
        })
    }

    fn description(&mut self) -> &'a str {
        let first = self.tokens.next();

        match first {
            Some(token) if token.kind == TokenKind::EOL => "",
            None => "",
            Some(token) => {
                let start = token.start;
                let mut end = start + token.len;
                for token in self.tokens.by_ref() {
                    if token.kind == TokenKind::EOL {
                        break;
                    } else {
                        end = token.start + token.len;
                    }
                }

                &self.source[start..end]
            }
        }
    }

    fn postings(&mut self) -> Result<Vec<Posting<'a>>> {
        let mut postings = Vec::new();
        self.eat_while_eol();
        while let Some(posting) = self.posting() {
            postings.push(posting?);
            self.eat_while_eol();
        }

        Ok(postings)
    }

    fn posting(&mut self) -> Option<Result<Posting<'a>>> {
        let account = self.account()?;
        match self.amount() {
            Ok(amount) => Some(Ok(Posting { account, amount })),
            Err(err) => Some(Err(err)),
        }
    }

    fn account(&mut self) -> Option<Account<'a>> {
        let account_kind_token = self.tokens.peek()?;
        let account_kind = match account_kind_token.kind {
            TokenKind::Assets => AccountKind::Assets,
            TokenKind::Equity => AccountKind::Equity,
            TokenKind::Expenses => AccountKind::Expenses,
            TokenKind::Income => AccountKind::Income,
            TokenKind::Liabilities => AccountKind::Liabilities,
            _ => return None,
        };
        let start = account_kind_token.start;
        let mut end = start + account_kind_token.len;
        self.tokens.next();

        while let Some(token) = self.tokens.peek() {
            match token.kind {
                TokenKind::Word
                | TokenKind::Assets
                | TokenKind::Equity
                | TokenKind::Expenses
                | TokenKind::Income
                | TokenKind::Liabilities
                | TokenKind::Colon => {
                    end = token.start + token.len;
                    self.tokens.next();
                }
                _ => break,
            }
        }

        Some(Account {
            name: &self.source[start..end],
            kind: account_kind,
        })
    }

    fn amount(&mut self) -> Result<Amount> {
        let currency = self.currency()?;
        let value = self.value()?;

        Ok(Amount { currency, value })
    }

    fn currency(&mut self) -> Result<Currency> {
        self.expect(&[TokenKind::GreatBritishPound])?;

        Ok(Currency::GreatBritishPound)
    }

    fn value(&mut self) -> Result<f64> {
        let is_negative = self
            .tokens
            .next_if(|token| token.kind == TokenKind::Dash)
            .is_some();
        let value = self.expect(&[TokenKind::Integer, TokenKind::Float])?;
        let value: f64 = self.source[value.start..value.start + value.len]
            .parse()
            .map_err(|_| ParserError {
                kind: ParserErrorKind::ParseFloatError,
                span: Span {
                    start: value.start,
                    len: value.len,
                },
            })?;

        if is_negative {
            Ok(-value)
        } else {
            Ok(value)
        }
    }

    fn expect(&mut self, expected: &[TokenKind]) -> Result<Token> {
        match self.tokens.next() {
            Some(token) if expected.contains(&token.kind) => Ok(token),
            Some(token) => Err(ParserError {
                kind: ParserErrorKind::UnexpectedToken {
                    expected: expected.into(),
                    got: Some(token.kind),
                },
                span: Span {
                    start: token.start,
                    len: token.len,
                },
            }),
            None => Err(ParserError {
                kind: ParserErrorKind::UnexpectedToken {
                    expected: expected.into(),
                    got: None,
                },
                span: Span {
                    start: self.source.len(),
                    len: 0,
                },
            }),
        }
    }

    fn eat_while_eol(&mut self) {
        while let Some(token) = self.tokens.peek() {
            if token.kind == TokenKind::EOL {
                self.tokens.next();
            } else {
                break;
            }
        }
    }
}

impl<'a> Iterator for Parser<'a> {
    type Item = Result<Transaction<'a>>;

    fn next(&mut self) -> Option<Self::Item> {
        self.next_transaction()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_creates_parser() {
        let source = "source";

        let _parser = Parser::new(source);
    }

    #[test]
    fn next_no_source_returns_none() {
        let source = "";
        let mut parser = Parser::new(source);

        let result = parser.next();

        assert_eq!(None, result);
    }

    #[test]
    fn next_transaction_no_date_returns_error() {
        let source = "not a date";
        let mut parser = Parser::new(source);

        let result = parser.next();

        let expected = Some(Err(ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Integer],
                got: Some(TokenKind::Word),
            },
            span: Span { start: 0, len: 3 },
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn next_transaction_no_description_returns_transaction() {
        let source = "2022-03-13";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 13),
            description: "",
            postings: Vec::new(),
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_single_digit_month_and_day_returns_transaction() {
        let source = "2022-1-1";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 1, 1),
            description: "",
            postings: Vec::new(),
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_only_year_in_date_returns_error() {
        let source = "2022";
        let mut parser = Parser::new(source);

        let result = parser.next();

        let expected = Some(Err(ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Dash],
                got: None,
            },
            span: Span { start: 4, len: 0 },
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn next_transaction_only_year_and_dash_in_date_returns_error() {
        let source = "2022-";
        let mut parser = Parser::new(source);

        let result = parser.next();

        let expected = Some(Err(ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Integer],
                got: None,
            },
            span: Span { start: 5, len: 0 },
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn next_transaction_no_day_in_date_returns_error() {
        let source = "2022-03";
        let mut parser = Parser::new(source);

        let result = parser.next();

        let expected = Some(Err(ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Dash],
                got: None,
            },
            span: Span { start: 7, len: 0 },
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn next_transaction_no_day_has_dash_in_date_returns_error() {
        let source = "2022-03-";
        let mut parser = Parser::new(source);

        let result = parser.next();

        let expected = Some(Err(ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Integer],
                got: None,
            },
            span: Span { start: 8, len: 0 },
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn next_date_has_dots_instead_of_dashes_returns_error() {
        let source = "2022.03.16";
        let mut parser = Parser::new(source);

        let result = parser.next();

        let expected = Some(Err(ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Integer],
                got: Some(TokenKind::Float),
            },
            span: Span { start: 0, len: 7 },
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn next_transaction_with_description_returns_transaction() {
        let source = "2022-03-13 description of transaction";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 13),
            description: "description of transaction",
            postings: Vec::new(),
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_with_description_ended_with_newline_returns_transaction() {
        let source = "2022-03-13 description of transaction\n";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 13),
            description: "description of transaction",
            postings: Vec::new(),
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_newline_before_transaction_returns_transaction() {
        let source = "
            2022-03-13";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 13),
            description: "",
            postings: Vec::new(),
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_with_posting_returns_transaction() {
        let source = "2022-03-19
            assets:piggy bank:account £500
        ";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 19),
            description: "",
            postings: vec![Posting {
                account: Account {
                    kind: AccountKind::Assets,
                    name: "assets:piggy bank:account",
                },
                amount: Amount {
                    currency: Currency::GreatBritishPound,
                    value: 500.0,
                },
            }],
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_with_posting_with_decimal_value_returns_transaction() {
        let source = "2022-03-20
            assets:piggy bank:account £11.11
        ";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 20),
            description: "",
            postings: vec![Posting {
                account: Account {
                    kind: AccountKind::Assets,
                    name: "assets:piggy bank:account",
                },
                amount: Amount {
                    currency: Currency::GreatBritishPound,
                    value: 11.11,
                },
            }],
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_with_posting_with_negative_value_returns_transaction() {
        let source = "2022-03-20
            assets:piggy bank:account £-500
        ";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 20),
            description: "",
            postings: vec![Posting {
                account: Account {
                    kind: AccountKind::Assets,
                    name: "assets:piggy bank:account",
                },
                amount: Amount {
                    currency: Currency::GreatBritishPound,
                    value: -500.0,
                },
            }],
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_with_posting_with_negative_value_with_space_returns_transaction() {
        let source = "2022-03-20
            assets:piggy bank:account £- 500
        ";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 20),
            description: "",
            postings: vec![Posting {
                account: Account {
                    kind: AccountKind::Assets,
                    name: "assets:piggy bank:account",
                },
                amount: Amount {
                    currency: Currency::GreatBritishPound,
                    value: -500.0,
                },
            }],
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_with_newline_before_posting_returns_transaction() {
        let source = "2022-03-20

            assets:piggy bank:account £- 500
        ";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 20),
            description: "",
            postings: vec![Posting {
                account: Account {
                    kind: AccountKind::Assets,
                    name: "assets:piggy bank:account",
                },
                amount: Amount {
                    currency: Currency::GreatBritishPound,
                    value: -500.0,
                },
            }],
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn next_transaction_with_all_account_kinds_returns_transaction() {
        let source = "
            2022-03-20 some complicated transaction
                assets:piggy bank:account         £ 899.50
                equity:piggy bank:savings account £-500

                expenses:food:groceries           £ 150.50
                income:salary                     £-500
                liabilities:loan                  £- 50.
        ";
        let mut parser = Parser::new(source);

        let transaction = parser.next();

        let expected = Some(Ok(Transaction {
            date: NaiveDate::from_ymd(2022, 3, 20),
            description: "some complicated transaction",
            postings: vec![
                Posting {
                    account: Account {
                        kind: AccountKind::Assets,
                        name: "assets:piggy bank:account",
                    },
                    amount: Amount {
                        currency: Currency::GreatBritishPound,
                        value: 899.50,
                    },
                },
                Posting {
                    account: Account {
                        kind: AccountKind::Equity,
                        name: "equity:piggy bank:savings account",
                    },
                    amount: Amount {
                        currency: Currency::GreatBritishPound,
                        value: -500.,
                    },
                },
                Posting {
                    account: Account {
                        kind: AccountKind::Expenses,
                        name: "expenses:food:groceries",
                    },
                    amount: Amount {
                        currency: Currency::GreatBritishPound,
                        value: 150.50,
                    },
                },
                Posting {
                    account: Account {
                        kind: AccountKind::Income,
                        name: "income:salary",
                    },
                    amount: Amount {
                        currency: Currency::GreatBritishPound,
                        value: -500.,
                    },
                },
                Posting {
                    account: Account {
                        kind: AccountKind::Liabilities,
                        name: "liabilities:loan",
                    },
                    amount: Amount {
                        currency: Currency::GreatBritishPound,
                        value: -50.,
                    },
                },
            ],
        }));
        assert_eq!(expected, transaction);
    }

    #[test]
    fn collect_returns_all_transactions() {
        let source = "
            2022-03-20 opening balances
                assets:piggy bank:account £ 500
                equity:opening balances   £-500

            2022-03-20 shopping
                expenses:food:groceries   £ 44.44
                assets:piggy bank:account £-44.44

            2022-03-20 salary

                assets:piggy bank:account £ 500
                income:salary             £-500

            2022-03-20 loan
                assets:piggy bank:account   £ 250.50

                liabilities:piggy bank:loan £-250.50
        ";
        let parser = Parser::new(source);

        let transaction: Result<Vec<_>> = parser.collect();

        let expected = Ok(vec![
            Transaction {
                date: NaiveDate::from_ymd(2022, 3, 20),
                description: "opening balances",
                postings: vec![
                    Posting {
                        account: Account {
                            kind: AccountKind::Assets,
                            name: "assets:piggy bank:account",
                        },
                        amount: Amount {
                            currency: Currency::GreatBritishPound,
                            value: 500.,
                        },
                    },
                    Posting {
                        account: Account {
                            kind: AccountKind::Equity,
                            name: "equity:opening balances",
                        },
                        amount: Amount {
                            currency: Currency::GreatBritishPound,
                            value: -500.,
                        },
                    },
                ],
            },
            Transaction {
                date: NaiveDate::from_ymd(2022, 3, 20),
                description: "shopping",
                postings: vec![
                    Posting {
                        account: Account {
                            kind: AccountKind::Expenses,
                            name: "expenses:food:groceries",
                        },
                        amount: Amount {
                            currency: Currency::GreatBritishPound,
                            value: 44.44,
                        },
                    },
                    Posting {
                        account: Account {
                            kind: AccountKind::Assets,
                            name: "assets:piggy bank:account",
                        },
                        amount: Amount {
                            currency: Currency::GreatBritishPound,
                            value: -44.44,
                        },
                    },
                ],
            },
            Transaction {
                date: NaiveDate::from_ymd(2022, 3, 20),
                description: "salary",
                postings: vec![
                    Posting {
                        account: Account {
                            kind: AccountKind::Assets,
                            name: "assets:piggy bank:account",
                        },
                        amount: Amount {
                            currency: Currency::GreatBritishPound,
                            value: 500.,
                        },
                    },
                    Posting {
                        account: Account {
                            kind: AccountKind::Income,
                            name: "income:salary",
                        },
                        amount: Amount {
                            currency: Currency::GreatBritishPound,
                            value: -500.,
                        },
                    },
                ],
            },
            Transaction {
                date: NaiveDate::from_ymd(2022, 3, 20),
                description: "loan",
                postings: vec![
                    Posting {
                        account: Account {
                            kind: AccountKind::Assets,
                            name: "assets:piggy bank:account",
                        },
                        amount: Amount {
                            currency: Currency::GreatBritishPound,
                            value: 250.50,
                        },
                    },
                    Posting {
                        account: Account {
                            kind: AccountKind::Liabilities,
                            name: "liabilities:piggy bank:loan",
                        },
                        amount: Amount {
                            currency: Currency::GreatBritishPound,
                            value: -250.50,
                        },
                    },
                ],
            },
        ]);
        assert_eq!(expected, transaction);
    }
}
