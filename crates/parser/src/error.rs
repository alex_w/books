use scanner::TokenKind;
use std::{cmp::Ordering, error::Error, fmt::Display};

pub type Result<T> = std::result::Result<T, ParserError>;

#[derive(Debug, PartialEq)]
pub struct ParserError {
    pub kind: ParserErrorKind,
    pub span: Span,
}

#[derive(Debug, PartialEq)]
pub enum ParserErrorKind {
    UnexpectedToken {
        expected: Vec<TokenKind>,
        got: Option<TokenKind>,
    },
    ParseDateError,
    ParseFloatError,
}

#[derive(Debug, PartialEq)]
pub struct Span {
    pub start: usize,
    pub len: usize,
}

impl Display for ParserError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.kind {
            ParserErrorKind::UnexpectedToken { expected, got } => {
                write!(f, "Unexpected token, expected ")?;

                let expected_len = expected.len();
                if expected_len > 1 {
                    write!(f, "one of ")?;
                }
                for (i, expected) in expected.iter().enumerate() {
                    write!(f, "`{expected}`")?;
                    if expected_len > 1 {
                        let second_last_index = expected_len - 2;
                        match i.cmp(&second_last_index) {
                            Ordering::Less => write!(f, ", ")?,
                            Ordering::Equal => write!(f, " or ")?,
                            Ordering::Greater => {}
                        }
                    }
                }
                if let Some(got) = got {
                    write!(f, " got `{got}`.")
                } else {
                    write!(f, " got end of file.")
                }
            }
            ParserErrorKind::ParseDateError => write!(f, "Invalid date."),
            ParserErrorKind::ParseFloatError => write!(f, "Invalid float."),
        }
    }
}

impl Error for ParserError {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn to_string_unexpected_end_of_file() {
        let error = ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Dash],
                got: None,
            },
            span: Span { start: 0, len: 1 },
        };

        let error = error.to_string();

        let expected = "Unexpected token, expected `-` got end of file.";
        assert_eq!(expected, error);
    }

    #[test]
    fn to_string_unexpected_token() {
        let error = ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Dash],
                got: Some(TokenKind::Colon),
            },
            span: Span { start: 0, len: 1 },
        };

        let error = error.to_string();

        let expected = "Unexpected token, expected `-` got `:`.";
        assert_eq!(expected, error);
    }

    #[test]
    fn to_string_expected_multiple_tokens() {
        let error = ParserError {
            kind: ParserErrorKind::UnexpectedToken {
                expected: vec![TokenKind::Dash, TokenKind::Colon, TokenKind::Assets],
                got: None,
            },
            span: Span { start: 0, len: 1 },
        };

        let error = error.to_string();

        let expected = "Unexpected token, expected one of `-`, `:` or `assets` got end of file.";
        assert_eq!(expected, error);
    }

    #[test]
    fn to_string_parse_date_error() {
        let error = ParserError {
            kind: ParserErrorKind::ParseDateError,
            span: Span { start: 0, len: 9 },
        };

        let error = error.to_string();

        assert_eq!("Invalid date.", error);
    }

    #[test]
    fn to_string_parse_float_error() {
        let error = ParserError {
            kind: ParserErrorKind::ParseFloatError,
            span: Span { start: 0, len: 3 },
        };

        let error = error.to_string();

        assert_eq!("Invalid float.", error);
    }
}
